﻿using System;
using NLog;

namespace Raise.Workbench.Service {
    public class QuartzServiceFactory {
        // ReSharper disable once UnusedMember.Local
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger(typeof(QuartzServiceFactory));

        public static QuartzService CreateServer() {
            string typeName = Configuration.ServerImplementationType;

            Type t = Type.GetType(typeName, true);
            QuartzService retValue = (QuartzService)Activator.CreateInstance(t);
            return retValue;
        }
    }
}